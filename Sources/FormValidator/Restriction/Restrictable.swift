//
//  Created on 04.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import Foundation

public protocol Restrictable: AnyObject {
    var restrictionRules: [RestrictableRule] { get set }
}

public extension Restrictable {
    func checkRestriction(text: String) -> Bool {
        return self.restrictionRules.allSatisfy({ $0.validate(text) })
    }
}
