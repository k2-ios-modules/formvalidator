//
//  Created on 04.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import Foundation

public protocol CheckHasChangable: HasChangable {
    associatedtype Value: Equatable

    var checkedValue: (() -> Value?)? { get set }
}

public extension CheckHasChangable {
    func checkChanged(value: Value) {
        guard let checkedValue = self.checkedValue else { return }
        self.hasChange.send(checkedValue() != value)
    }
}
