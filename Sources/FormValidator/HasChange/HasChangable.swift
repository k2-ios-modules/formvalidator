//
//  Created on 04.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import Combine
import Foundation

public protocol HasChangable: AnyObject {
    typealias ChangeSubject = CurrentValueSubject<Bool, Never>

    var hasChange: ChangeSubject { get }
}
