//
//  Created on 04.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import Combine
import Foundation

public final class FormValidator: StateValidatable, HasChangable {

    public typealias DependentItem = (main: StateValidatable, dependent: StateValidatable)

    // MARK: - variables

    private var cancellables: Set<AnyCancellable> = []

    private var items: [StateValidatable] = []

    private var dependentItems: [DependentItem] = []

    // MARK: - properties

    public private(set) var isValid: ValidSubject = .init(false)

    public private(set) var hasChange: ChangeSubject = .init(false)

    // MARK: - init

    public init() { }

    // MARK: - sink

    private func sinkItem(_ item: StateValidatable) {
        item.isValid
            .receive(on: DispatchQueue.main)
            .sink { [weak self] _ in self?.checkValidation() }
            .store(in: &self.cancellables)
    }

    private func sinkAll() {
        self.cancellables.removeAll()
        self.items.forEach(self.sinkItem(_:))
        self.dependentItems.forEach({ item in
            item.main.isValid
                .dropFirst()
                .filter({ $0 })
                .sink { _ in _ = item.dependent.validate() }
                .store(in: &self.cancellables)
        })
    }

    // MARK: - setters

    private func addItem(_ item: StateValidatable) -> Bool {
        guard self.items.contains(where: { $0 === item }) == false
        else { return false }
        self.items.append(item)
        return true
    }

    public func addItem(_ item: StateValidatable) {
        guard self.addItem(item) else { return }
        self.sinkItem(item)
    }

    public func addItems(_ items: [StateValidatable]) {
        items.forEach({
            guard self.addItem($0) else { return }
        })
        self.sinkAll()
    }

    public func setItems(
        _ items: [StateValidatable],
        dependentItems: [DependentItem] = []
    ) {
        self.items = items
        self.dependentItems = dependentItems
        self.sinkAll()
    }

    private func removeItem(_ item: StateValidatable) -> Bool {
        guard let index = self.items.firstIndex(where: { $0 === item })
        else { return false }
        self.items.remove(at: index)
        return true
    }

    public func removeItem(_ item: StateValidatable) {
        guard self.removeItem(item) else { return }
        self.sinkAll()
    }

    public func removeItems(_ items: [StateValidatable]) {
        items.forEach({
            guard self.removeItem($0) else { return }
        })
        self.sinkAll()
    }

    // MARK: - validation

    private func checkValidation() {
        let invalidItem = self.items.first(where: { $0.isValid.value == false })
        let isValid = invalidItem == nil
        let hasChange = self.items
            .compactMap({ $0 as? HasChangable })
            .contains(where: \.hasChange.value)

        self.isValid.send(isValid)
        self.hasChange.send(hasChange)
    }

    @discardableResult
    public func validate() -> Bool {
        var isValid: Bool = true
        self.items.forEach({
            // need call `validate` for all items
            if $0.validate() == false, isValid {
                isValid = false
            }
        })
        return isValid
    }

    public func silentValidate() {
        self.items.forEach({ $0.silentValidate() })
    }
}

