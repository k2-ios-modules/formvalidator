//
//  Created on 04.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import Foundation

public protocol RequiredValidatable: Validatable {
    var isRequred: Bool { get }
}
