//
//  Created on 04.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import Foundation

public protocol Validatable: AnyObject {
    func validate() -> Bool
    func silentValidate()
}

public extension Validatable {
    func silentValidate() {
        _ = self.validate()
    }
}
