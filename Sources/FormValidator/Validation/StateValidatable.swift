//
//  Created on 04.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import Combine
import Foundation

public protocol StateValidatable: Validatable {
    typealias ValidSubject = CurrentValueSubject<Bool, Never>

    var isValid: ValidSubject { get }
}
