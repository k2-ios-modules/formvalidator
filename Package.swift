// swift-tools-version: 5.7

import PackageDescription

let package = Package(
    name: "FormValidator",
    platforms: [
        .iOS(.v13)
    ],
    products: [
        .library(
            name: "FormValidator",
            targets: ["FormValidator"]
        )
    ],
    targets: [
        .target(
            name: "FormValidator"
        )
    ]
)
